package com.test.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.model.Student;
import com.test.model.Students;

@Repository
@Service
public class StudentService {
	
	private static Students std = new Students();
	
	static
	{
		std.getStdList().add(new Student(101, "Ranjan", "Kolkata"));
		std.getStdList().add(new Student(102, "Rakesh", "Blr"));
		std.getStdList().add(new Student(103, "Ramanuj", "Chen"));
		std.getStdList().add(new Student(104, "Ram", "Hyd"));
		std.getStdList().add(new Student(105, "Krishna", "TPT"));
	}
	
	//read student data
	public Students getAllStudents()
	{
		return std;
	}
	
	//create student data
	public void addStudent(Student obj)
	{
		std.getStdList().add(obj);
	}
	
	//update student data
	public String updateStudent(Student obj)
	{
		for(int i = 0; i < std.getStdList().size(); i++)
		{
			Student obj1 = std.getStdList().get(i);
			
			if(obj1.getId().equals(obj.getId()))
			{
				std.getStdList().set(i, obj);
				
				System.out.println("one record updated");
			}
		}
		
		return "the given id is not available";
	}
	
	//delete student data
	public String deleteStudent(Integer id)
	{
		for(int i = 0; i < std.getStdList().size(); i++)
		{
			Student obj = std.getStdList().get(i);
			
			if(obj.getId().equals(id))
			{
				std.getStdList().remove(i);
				
				System.out.println("one record deleted");
			}
		}
		
		return "the given id is not available";
	}
}
