package com.test.model;

import java.util.ArrayList;
import java.util.List;

public class Students {
	
	private List<Student> stdList = null;

	public List<Student> getStdList() {
		
		if(stdList == null)
		{
			stdList = new ArrayList<>();
		}
		
		return stdList;
	}

	public void setStdList(List<Student> stdList) {
		this.stdList = stdList;
	}
}
