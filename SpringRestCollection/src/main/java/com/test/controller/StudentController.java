package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Student;
import com.test.model.Students;
import com.test.service.StudentService;

@RestController
public class StudentController {
	
	@Autowired
	private StudentService stdService;
	
	@GetMapping(value = "/listStudents", produces = "application/json")
	public Students getAllStudents()
	{
		return stdService.getAllStudents();
	}
	
	@PostMapping(value = "/addStudent", consumes = "application/json")
	public Students createStudent(@RequestBody Student std)
	{
		int id = stdService.getAllStudents().getStdList().size() + 1;
		
		std.setId(id);
		
		stdService.addStudent(std);
		
		return stdService.getAllStudents();
	}
	
	@PutMapping(value = "/update/{id}", consumes = "application/json")
	public Students updateStudent(@RequestBody Student std, @PathVariable("id") Integer id)
	{
		std.setId(id);
		
		stdService.updateStudent(std);
		
		return stdService.getAllStudents();
	}
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public Students deleteStudent(@PathVariable("id") Integer id)
	{
		stdService.deleteStudent(id);
		
		return stdService.getAllStudents();
	}
}
